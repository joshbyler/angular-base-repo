'use strict';

var gulp = require('gulp');
var util = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var bower = require('gulp-bower');
var karma = require('karma').server;
var protractor = require("gulp-protractor").protractor;
var shell = require('gulp-shell');
var path = require('path');

var config = {
  cssDir: './app/stylesheets/css'
};

/** CSS **/
gulp.task('css', function() {
return gulp.src('app/stylesheets/scss/*.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest(config.cssDir));
});

/** JS **/
gulp.task('js', function() {
    return gulp.src([
        'app/bower_components/angular/angular.js',
        'app/bower_components/angular-loader/angular-loader.js',
        'app/bower_components/angular-mocks/*.js',
        'app/bower_components/angular-route/angular-route.js',
        'app/bower_components/html5-boilerplate/src/js/main.js',
        'app/bower_components/html5-boilerplate/src/js/plugins.js',
        'app/bower_components/html5-boilerplate/src/js/vendor/*.js',
        'app/bower_components/lodash/lodash.js',
        'app/bower_components/restangular/dis/restangular.min.js'
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('app/js'));
});

/** Watch **/
gulp.task('watch', function() {
    return gulp.watch(['app/stylesheets/scss/**/*.scss'],['css']);
});

/** Boot **/
gulp.task('boot', ['css', 'js', 'watch']);

/** Default **/
gulp.task('default', ['boot']);
